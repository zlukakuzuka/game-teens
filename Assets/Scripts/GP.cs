﻿namespace Assets.Scripts {
    public class GP {
        public const string nameSceneMainMenu = "startScreen";
        public const string nameSceneGame = "game";
        public const int portionOfMeds = 3;
        public const int totalAmountMeds = 100;
        public const int lives = 100;
        public const string stateOfSound = "State of sound";
    }
}